
The Title
=========


The Subtitle
------------


### A Third-Level Heading


This is the first command


```bash
echo "hello, world"
```

```stdout
hello, world
```

This is a multi-line if block
It also has multiple lines of comments


```bash
MY_VAR="world"
if [ -n "$MY_VAR" ] ; then
    echo -e "hello, ${MY_VAR}\nwhat's up?" ;
fi
```

```stdout
hello, world
what's up?
```

This is the third block which does not produce any output


```bash
unset MY_VAR
if [ -n "$MY_VAR" ] ; then
    echo "hello, $MY_VAR"
fi
```


This is a for loop


```bash
for i in 1 2 3 4 ; do
    echo -n "Line "
    echo "$i"
done
```

```stdout
Line 1
Line 2
Line 3
Line 4
```

Here comes a line-break in a single command


```bash
echo "hello, \
world"
```

```stdout
hello, world
```

This is a comment-only block
with multiple lines


And  
another  
one  


```bash
:
```


The next command is an error


```bash
thisIsAnError arg1 arg2
```

```stderr
thisIsAnError: command not found

(exit 127)
```


*Generated from [test.bash](test.bash)*

.PHONY: test
test:
	./bash_to_markdown test.bash > .test-out.md
	diff .test-out.md test.md
	rm .test-out.md

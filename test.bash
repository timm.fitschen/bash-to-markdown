#!/bin/bash

# The Title
# =========

# The Subtitle
# ------------

# ### A Third-Level Heading

# This is the first command
echo "hello, world"

# This is a multi-line if block
# It also has multiple lines of comments
MY_VAR="world"
if [ -n "$MY_VAR" ] ; then
    echo -e "hello, ${MY_VAR}\nwhat's up?" ;
fi

# This is the third block which does not produce any output
unset MY_VAR
if [ -n "$MY_VAR" ] ; then
    echo "hello, $MY_VAR"
fi

# This is a for loop
for i in 1 2 3 4 ; do
    echo -n "Line "
    echo "$i"
done

# Here comes a line-break in a single command
echo "hello, \
world"


# This is a comment-only block
# with multiple lines

# And  
# another  
# one  

:

# The next command is an error
thisIsAnError arg1 arg2

# Bash to Markdown

> A Bash script which runs another bash script and formats the comments and the output in markdown.

## Usage

```bash
bash_to_markdown SCRIPT
```

This program runs the SCRIPT and converts the SCRIPT's lines and the SCRIPT's
output (stdout/stderr) into a markdown representation. It is intended for
generating markdown documentation for scripts which demonstrate examples of
bash code.

Warning: it *actually executes* the SCRIPT (basically line-by-line) and
prints all the input and output.

## Example

When you have bash script like this:

```
#!/bin/bash

# This is a comment
echo "hello, world"
```

`bash_to_markdown` will output this:

~~~
This is a comment

```bash
echo "hello, world"
```

```stdout
hello, world
```
~~~

As you can see, `bash_to_markdown` basically does three things.

1. It prints all comments without the leading `#`.

2. It also prints the command as it is written in the input script and puts it into markdown code fences.

3. It prints the actuall output of the command.

If an error occures during the execution


## Features and Limitations

`bash_to_markdown` is currently *very* limited. It is implemented to cover the
test cases in [test.bash](./test.bash) and nothing more.

## License (AGPLv3 or later)

Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
